FROM php:7.1-apache-jessie

# Let's install Git and Zip (for Composer)
RUN apt-get -y update && apt-get install -y zlib1g-dev git
RUN docker-php-ext-install opcache mbstring zip

# Let's install composer
ENV COMPOSER_ALLOW_SUPERUSER 1

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer &&\
    chmod +x /usr/local/bin/composer

# Do specific stuff like enabling PHP extensions, etc...
# ...

# Copy all the project to the root directory
COPY . /var/www/html/

RUN composer install
